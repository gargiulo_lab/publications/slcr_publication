### Phenotypic mapping of pathological crosstalk between glioblastoma and innate immune cells by synthetic genetic tracing

- The repository contains the pipeline and raw gene count matrix from the publication 

**NOTE:** sLCR pipeline is in the "original_pipeline" folder. Individual analyses (.zip) are in the "cis_regulatory_elements/analysis" folder. This folder also contains the cmd line to generate the reporters. 


> Molecular Oncology group ,  https://www.mdc-berlin.de/gargiulo
