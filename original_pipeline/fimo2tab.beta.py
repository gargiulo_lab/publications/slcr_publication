#!/usr/bin/python

""" 

Convert FIMO (txt) output to tabular format (compatible with PWMatch applications)

It saves the best p-value for each pair (region, pwm)

"""

# Tag
__author__ = "Iros Barozzi"
__where__ = "Ifom-ieo"
__ref__ = []

import sys
import time
import math
import commands
import string


# Parameters
# ----------------------------------------------------------------------------------------- #


# Input arguments
pwms = sys.argv[1] # in MEME format
fimo = sys.argv[2] # in txt format


# Script
# ----------------------------------------------------------------------------------------- #


def cleanSTR(string):
    """ Cleans a string from \r and \n """
    string = string.replace("\r","")
    string = string.replace("\n","")
    return string

def filewrite(outputfile,text):
    """ Writes the content of text in outputfile file """
    f = open(outputfile,'w')
    f.write(text)
    f.close()

motifs = []
f1 = open(pwms, 'r')
for l in f1.readlines():
	temp = l.split("MOTIF")
	if len(temp) == 2:
		motifs.append(cleanSTR(temp[1]).replace(" ", ""))
f1.close()

regions_best = {}
f2 = open(fimo, 'r')
for l in f2.readlines():
	temp = l.split("\t")
	if temp[0] != "Motif":
		pwm = cleanSTR(temp[0])
		region = cleanSTR(temp[1])
		p = -1*math.log(float(cleanSTR(temp[6])),10)
		if not regions_best.has_key(region):
			regions_best[region] = {}
			for m in motifs:
				# log10(1) -> 0
				regions_best[region][m] = 0
		if p > regions_best[region][pwm]:
			regions_best[region][pwm] = p
f2.close()

out_best = "Region"
for m in motifs:
	out_best += "\tFimo_Best_"+m
out_best += "\n"
for r in regions_best:
	out_best += r
	for m in motifs:
		out_best += "\t"+str(regions_best[r][m])
	out_best += "\n"

filewrite(fimo+".best.tab", out_best)


# ----------------------------------------------------------------------------------------- #

