""" 

Libraries :: General functions

"""

# Tags
__author__ = "Iros Barozzi"
__where__ = "Ifom-ieo"
__ref__ = []

import math

# ----------------------------------------------------------------------------------------- #

def cleanSTR(string):
    """ Cleans a string from \r and \n """
    string = string.replace("\r","")
    string = string.replace("\n","")
    return string

def filewrite(outputfile,text):
    """ Writes the content of text in outputfile file """
    f = open(outputfile,'w')
    f.write(text)
    f.close()
    
def meanCalc(list_values):
    """ Takes a list of elements and return the mean of the float values """
    i = 0
    sum = float(0)
    for el in list_values:
        i = i + 1
	sum = sum + float(el)
    if i > 0:
        return round((sum/float(i)), 6)
    else:
	return 0

def stddevCalc(list_values, mean):
    """ Takes a list of elements and the mean of them and return the std deviations """
    stddev_tmp = 0
    i = 0
    for el in list_values:
	i = i + 1
        stddev_tmp += float(((mean - float(el)) * (mean - float(el))))
    if i > 0:
        return round(math.sqrt(float(float(stddev_tmp) / float(i))), 6)
    else:
	return 0

def stderrCalc(stddev, n):
    """ Takes the std deviations and the number of objects and returns the std errors """
    return round(float(stddev) / math.sqrt(float(n)), 6)

def fact(x):
    """ Takes a number in input and returns factorial """
    res = 1
    for m in range(1,x+1):
        res = res * m
    return res

def binomio_newton(x,y): 
    """ Newton binomio with parameters x,y """
    return (fact(x) / (fact(y) * fact(x-y)))

def intervals_overlap(s1, e1, s2, e2):
    """ Overlaps 2 intervals """
    if e1 < s1 or e2 < s2:
	return 'Error: wrong intervals'
    else:
	if s1 <= s2 <= e1 or s1 <= e2 <= e1 or  s2 <= s1 <= e2 or s2 <= e1 <= e2:
	    return 1
	else:
	    return 0

def median(numList):
    vals = sorted(numList)
    if len(vals) > 0:
        if len(vals) % 2 == 1:
            return vals[(len(vals)+1)/2-1]
        else:
            lower = vals[len(vals)/2-1]
            upper = vals[len(vals)/2]
            return (float(lower + upper)) / 2
    else:
	return 0

def complement(seq):
	complement = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A'}
	complseq = [complement[base] for base in seq]
	return complseq

def reverse_complement(seq):
	seq = list(seq)
	seq.reverse()
	return ''.join(complement(seq))

# ----------------------------------------------------------------------------------------- #
