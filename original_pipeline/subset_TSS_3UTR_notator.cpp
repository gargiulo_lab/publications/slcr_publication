/*
 * NAME: subset_TSS_3UTR_notator.cpp
 * WHAT: take a UCSC table, filter it for the list of gene identifiers provided and perform annotation of the regions specified
 * WHO:  Iros Barozzi @ IEO
 * 
 * @INPUT: 	argv[1]: UCSC table
 *			argv[2]: gene identifiers list
 *			argv[3]: bed regions (chrom, start, end are compulsory, then any number of custom fields is allowed)
 *			argv[4]: output file path
 * 
 * @OUTPUT:	annotation table
 *
 */

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <deque>
#include <map>
#include <ctime>
#include <cstdlib>
#include <math.h>
#include <stdio.h>
#include <ctype.h>


using namespace std;


typedef struct
{
    string chrom;
    int start;
    int end;
	string strand;
	string name1;
	string name2;
    string line;
    long int distance_TSS_1st;
	long int distance_TSS_2nd;
    long int distance_TES_1st;
	long int distance_TES_2nd;
	string gene_TSS_1st_ID1;
	string gene_TSS_1st_ID2;
	string gene_TSS_2nd_ID1;
	string gene_TSS_2nd_ID2;
	string gene_TES_1st_ID1;
	string gene_TES_1st_ID2;
	string gene_TES_2nd_ID1;
	string gene_TES_2nd_ID2;
} region;


/*
 * WHAT :: same as python split
 */
inline void tokenize(const string& str, vector<string>& tokens, const string& delimiters = " ")
{
    // Skip delimiters at beginning.
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    string::size_type pos     = str.find_first_of(delimiters, lastPos);
    while (string::npos != pos || string::npos != lastPos)
    {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }
}

/*
 * WHAT :: change each element of the string to upper case
 */
string StringToUpper(string strToConvert)
{
   for(unsigned int i=0;i<strToConvert.length();i++)
   {
      strToConvert[i] = toupper(strToConvert[i]);
   }
   return strToConvert;
}

/*
 * WHAT :: change each element of the string to lower case
 */
string StringToLower(string strToConvert)
{
   for(unsigned int i=0;i<strToConvert.length();i++)
   {
      strToConvert[i] = tolower(strToConvert[i]);
   }
   return strToConvert;
}

/*
 * WHAT :: check if a substring is present in a given string
 */
bool find_substring(string source, const string find)
{
    size_t j;
    for ( ; (j = source.find( find )) != string::npos ; ) 
    {
        return 1;
    }
    return 0;
}

/*
 * WHAT :: same as python replace
 */
void find_and_replace(string &source, const string find, string replace)
{
    size_t j;
    for ( ; (j = source.find( find )) != string::npos ; ) 
    {
        source.replace( j, find.length(), replace );
    }
}

/*
 * WHAT :: convert strand symbols to +/-
 */
string strand_converter(string input_strand)
{
    if (input_strand=="F" || input_strand=="+") return "+";
    if (input_strand=="R" || input_strand=="-") return "-";
}

/*
 * WHAT :: comparison sorting function
 */
bool compare(const region &region1, const region &region2)
{
    return region1.start < region2.start;
}

struct mycompare {
    bool operator() (const region &region1, const region &region2)
    {
        return region1.start < region2.start;
    }
};

/*
 * WHAT :: overlap two genomic intervals
 */
bool overlap(const region &region1, const region &region2)
{
    if (region1.chrom != region2.chrom)
        return 0;
    else {
        if ( (region1.start <= region2.end) && (region2.start <= region1.end) )
            return 1;
        else
            return 0;
    }
}

/*
 * WHAT :: clear a region variable
 */
void region_clear(region &tmp_region)
{
	tmp_region.chrom = "";
    tmp_region.start = 0;
    tmp_region.end = 0;
    tmp_region.strand = "";
	tmp_region.name1 = "";
	tmp_region.name2 = "";
	tmp_region.line = "";
    tmp_region.distance_TSS_1st = 0;
    tmp_region.distance_TSS_2nd = 0;
	tmp_region.distance_TES_1st = 0;
    tmp_region.distance_TES_2nd = 0;
}


int main (int argc, char * const argv[]) {

 string outfile_path = argv[4];
 
 string line;
 vector<string> v;

 int i;
 string colname, header;
 int name_col, name2_col, start_col, end_col, strand_col, chrom_col;

 region tmp_region;
 region tmp_region_TSS;
 region tmp_region_TES;
 
 int tmp_peak_center, multiplier;
	
 map<string, int> gene_IDs;

 map<string, vector<region> > TSS_regions;
 map<string, vector<region> >::iterator TSS_regions_it;
 map<string, vector<region> > TES_regions;
 map<string, vector<region> >::iterator TES_regions_it;
 map<string, vector<region> > input_regions;
 map<string, vector<region> >::iterator input_regions_it;

 vector<region>::iterator TSS_regions_inner_it;
 vector<region>::iterator TES_regions_inner_it;
 vector<region>::iterator input_inner_it;

 /*** LOAD list of gene identifiers ***/
 printf("%s\n", "Reading gene identifiers");
 ifstream primfile(argv[2]);
 while (getline(primfile, line)) {
	 v.clear();
	 tokenize(line, v, "\t");
	 if (v[0].substr(0,1) != "#" and v[0].substr(0,1) != "\n") gene_IDs[v[0].c_str()] = 0;
 }
 primfile.close();
 
 /*** LOAD UCSC table ***/
 printf("%s\n", "Reading UCSC table");
 primfile.open(argv[1]);
 while (getline(primfile, line)) {
     v.clear();
     tokenize(line, v, "\t");
	 // retrieve fields from the header
	 if (v[0].substr(0,1) == "#") {
		 i = 0;
		 while (i < v.size()) {
			 colname = v[i];
			 find_and_replace(colname, "#", "");
			 if (colname == "name") name_col = i;
			 else if (colname == "name2") name2_col = i;
			 else if (colname == "chrom") chrom_col = i;
			 else if (colname == "strand") strand_col = i;
			 else if (colname == "txStart") start_col = i;
			 else if (colname == "txEnd") end_col = i;
			 i += 1;
		 }
     } else if (v[0].substr(0,1) != "#") {
         tmp_region.chrom = v[chrom_col];
         tmp_region.start = atoi(v[start_col].c_str());
         tmp_region.end = atoi(v[end_col].c_str());
		 tmp_region.strand = strand_converter(v[strand_col].c_str());
		 tmp_region.name1 = v[name_col];
		 tmp_region.name2 = v[name2_col];
		 if (gene_IDs.find(tmp_region.name1) != gene_IDs.end() or gene_IDs.find(tmp_region.name2) != gene_IDs.end()) {
			 // if +, start is TSS, end is TES
			 if (tmp_region.strand == "+") {
				 tmp_region_TSS = tmp_region;
				 tmp_region_TES = tmp_region;
				 tmp_region_TSS.end = tmp_region.start;
				 TSS_regions[tmp_region.chrom].push_back(tmp_region_TSS);
				 tmp_region_TES.start = tmp_region.end;
				 TES_regions[tmp_region.chrom].push_back(tmp_region_TES);
			 // if -, start is TES, end is TSS
			 } else {
				 tmp_region_TSS = tmp_region;
				 tmp_region_TES = tmp_region;
				 tmp_region_TSS.start = tmp_region.end;
				 TSS_regions[tmp_region.chrom].push_back(tmp_region_TSS);
				 tmp_region_TES.end = tmp_region.start;
				 TES_regions[tmp_region.chrom].push_back(tmp_region_TES);
			 }
			 //printf("%s, %s, %s, %d, %d, TSS: %d, TES: %d, %s\n", tmp_region.name1.c_str(), tmp_region.name2.c_str(), tmp_region.chrom.c_str(), tmp_region.start, tmp_region.end, tmp_region_TSS.start, tmp_region_TES.start, tmp_region.strand.c_str());
		 }
    }
 }
 primfile.close();
 
 /*** LOAD regions ***/
 printf("%s\n", "Reading peaks");
 primfile.open(argv[3]);
 while (getline(primfile, line)) { // for each line of the file
     v.clear();
     tokenize(line, v, "\t");
	 if (v[1] == "start") header = line;
     else if (v[0].substr(0,1) != "#" and v[1] != "start" and v[0].substr(0,1) != "\n") {
         tmp_region.chrom = v[0];
		 tmp_region.start = atoi(v[1].c_str());
		 tmp_region.end = atoi(v[2].c_str());
		 tmp_region.line = line;
         input_regions[tmp_region.chrom].push_back(tmp_region);
    }
 }
 primfile.close();

 /*** SORT the MAPS ***/
 printf("%s\n", "Sorting");
 for (TSS_regions_it=TSS_regions.begin(); TSS_regions_it != TSS_regions.end(); TSS_regions_it++) // iteration over chrom
     sort(TSS_regions[(*TSS_regions_it).first].begin(), TSS_regions[(*TSS_regions_it).first].end(), mycompare()); // sort regions in chrom context
 for (input_regions_it=input_regions.begin(); input_regions_it != input_regions.end(); input_regions_it++) // iteration over chrom
     sort(input_regions[(*input_regions_it).first].begin(), input_regions[(*input_regions_it).first].end(), mycompare()); // sort regions in chrom context

 /*** COMPARISON ***/
 printf("%s\n", "Comparing");
 for (input_regions_it=input_regions.begin(); input_regions_it != input_regions.end(); input_regions_it++) { // iteration over chrom
	 for (input_inner_it=input_regions[(*input_regions_it).first].begin(); input_inner_it != input_regions[(*input_regions_it).first].end(); input_inner_it++) {
         (*input_inner_it).distance_TSS_1st = 1000000000;
		 (*input_inner_it).distance_TES_1st = 1000000000;
		 (*input_inner_it).distance_TSS_2nd = 1000000000;
		 (*input_inner_it).distance_TES_2nd = 1000000000;
		 (*input_inner_it).gene_TSS_1st_ID1 = "-";
		 (*input_inner_it).gene_TSS_1st_ID2 = "-";
		 (*input_inner_it).gene_TSS_2nd_ID1 = "-";
		 (*input_inner_it).gene_TSS_2nd_ID2 = "-";
		 (*input_inner_it).gene_TES_1st_ID1 = "-";
		 (*input_inner_it).gene_TES_1st_ID2 = "-";
		 (*input_inner_it).gene_TES_2nd_ID1 = "-";
		 (*input_inner_it).gene_TES_2nd_ID2 = "-";
         tmp_peak_center = ((*input_inner_it).start + (*input_inner_it).end) / 2;
         for (TSS_regions_inner_it=TSS_regions[(*input_regions_it).first].begin(); TSS_regions_inner_it != TSS_regions[(*input_regions_it).first].end(); TSS_regions_inner_it++) {
			 if ((*TSS_regions_inner_it).strand == "+") multiplier = 1;
			 else multiplier = -1;
             // better than the first?
             if (abs(tmp_peak_center - (*TSS_regions_inner_it).start) < abs((*input_inner_it).distance_TSS_1st)) {
				 // if so, first substitute the second with the first that is being replaced
				 (*input_inner_it).distance_TSS_2nd = (*input_inner_it).distance_TSS_1st;
				 (*input_inner_it).gene_TSS_2nd_ID1 = (*input_inner_it).gene_TSS_1st_ID1;
				 (*input_inner_it).gene_TSS_2nd_ID2 = (*input_inner_it).gene_TSS_1st_ID2;
				 // and then save the new best one
				 (*input_inner_it).distance_TSS_1st = multiplier * (tmp_peak_center - (*TSS_regions_inner_it).start);
				 (*input_inner_it).gene_TSS_1st_ID1 = (*TSS_regions_inner_it).name2;
				 (*input_inner_it).gene_TSS_1st_ID2 = (*TSS_regions_inner_it).name1;
			 // if not, better than second?
			 } else if (abs(tmp_peak_center - (*TSS_regions_inner_it).start) < abs((*input_inner_it).distance_TSS_2nd)) {
				 (*input_inner_it).distance_TSS_2nd = multiplier * (tmp_peak_center - (*TSS_regions_inner_it).start);
				 (*input_inner_it).gene_TSS_2nd_ID1 = (*TSS_regions_inner_it).name2;
				 (*input_inner_it).gene_TSS_2nd_ID2 = (*TSS_regions_inner_it).name1;
			 }
         }
		 for (TES_regions_inner_it=TES_regions[(*input_regions_it).first].begin(); TES_regions_inner_it != TES_regions[(*input_regions_it).first].end(); TES_regions_inner_it++) {
			 if ((*TES_regions_inner_it).strand == "-") multiplier = 1;
			 else multiplier = -1;
             // better than the first?
             if (abs(tmp_peak_center - (*TES_regions_inner_it).start) < abs((*input_inner_it).distance_TES_1st)) {
				 // if so, first substitute the second with the first that is being replaced
				 (*input_inner_it).distance_TES_2nd = (*input_inner_it).distance_TES_1st;
				 (*input_inner_it).gene_TES_2nd_ID1 = (*input_inner_it).gene_TES_1st_ID1;
				 (*input_inner_it).gene_TES_2nd_ID2 = (*input_inner_it).gene_TES_1st_ID2;
				 // and then save the new best one
				 (*input_inner_it).distance_TES_1st = multiplier * (tmp_peak_center - (*TES_regions_inner_it).start);
				 (*input_inner_it).gene_TES_1st_ID1 = (*TES_regions_inner_it).name2;
				 (*input_inner_it).gene_TES_1st_ID2 = (*TES_regions_inner_it).name1;
			 // if not, better than second?
			 } else if (abs(tmp_peak_center - (*TES_regions_inner_it).start) < abs((*input_inner_it).distance_TES_2nd)) {
				 (*input_inner_it).distance_TES_2nd = multiplier * (tmp_peak_center - (*TES_regions_inner_it).start);
				 (*input_inner_it).gene_TES_2nd_ID1 = (*TES_regions_inner_it).name2;
				 (*input_inner_it).gene_TES_2nd_ID2 = (*TES_regions_inner_it).name1;
			 }
         }
     }
 }
  
 /*** SAVE results ***/
 ofstream outfile;
 outfile.open(outfile_path.c_str());
 // header
 outfile << "#" << header;
 outfile << "\tbest_class\tbest_ID1\tbest_ID2\tbest_distance";
 outfile << "\tTSS_1st_gene_ID1\tTSS_1st_gene_ID2\tTSS_1st_distance\tTSS_2nd_gene_ID1\tTSS_2nd_gene_ID2\tTSS_2nd_distance";
 outfile << "\tTES_1st_gene_ID1\tTES_1st_gene_ID2\tTES_1st_distance\tTES_2nd_gene_ID1\tTES_2nd_gene_ID2\tTES_2nd_distance\n";
 for (input_regions_it=input_regions.begin(); input_regions_it != input_regions.end(); input_regions_it++) { // iteration over chrom
	 for (input_inner_it=input_regions[(*input_regions_it).first].begin(); input_inner_it != input_regions[(*input_regions_it).first].end(); input_inner_it++) {
		 outfile << (*input_inner_it).line;
		 // best candidate
		 if (abs((*input_inner_it).distance_TSS_1st) <= abs((*input_inner_it).distance_TES_1st))
			 outfile << "\tTSS\t" << (*input_inner_it).gene_TSS_1st_ID1 << "\t" << (*input_inner_it).gene_TSS_1st_ID2 << "\t" << (*input_inner_it).distance_TSS_1st;
		 else 
			 outfile << "\tTES\t" << (*input_inner_it).gene_TES_1st_ID1 << "\t" << (*input_inner_it).gene_TES_1st_ID2 << "\t" << (*input_inner_it).distance_TES_1st;
		 outfile << "\t" << (*input_inner_it).gene_TSS_1st_ID1 << "\t" << (*input_inner_it).gene_TSS_1st_ID2 << "\t" << (*input_inner_it).distance_TSS_1st;
		 outfile << "\t" << (*input_inner_it).gene_TSS_2nd_ID1 << "\t" << (*input_inner_it).gene_TSS_2nd_ID2 << "\t" << (*input_inner_it).distance_TSS_2nd;
		 outfile << "\t" << (*input_inner_it).gene_TES_1st_ID1 << "\t" << (*input_inner_it).gene_TES_1st_ID2 << "\t" << (*input_inner_it).distance_TES_1st;
		 outfile << "\t" << (*input_inner_it).gene_TES_2nd_ID1 << "\t" << (*input_inner_it).gene_TES_2nd_ID2 << "\t" << (*input_inner_it).distance_TES_2nd;
		 outfile << "\n";
	 }
 }
 outfile.close();

 return 0;

}
